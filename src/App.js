import React, {Component} from 'react';
import {getTodo, postTodo, deleteTodo, putTodo} from './rest';
import InputTodo from './components/input-todo';
import TotoList from './components/todo-list';
import logo from './logo.svg';
import './App.css'

class App extends Component {
  state = {
    todos: []
  };

  componentDidMount() {
    getTodo()
      .then(todos => {
        this.setState({
          todos
        });
      });
  }

  addTodo = title => {
    const todo = {
      id: Date.now(),
      title,
      completed: false
    };

    postTodo(todo);

    this.setState({
      todos: this.state.todos.concat([todo])
    });
  };

  deleteTodo = id => {
    const todos = this.state.todos.filter(todo => todo.id !== id);

    this.setState({
      todos
    });

    deleteTodo(id);
  };

  editTodo = (id, title) => {
    const todos = this.state.todos.map(todo => {
      if (todo.id === id) {
        todo.title = title;
      }
      return todo;
    });
    const index = todos.findIndex(todo => todo.id === id)

      this.setState({
      todos
    });

    putTodo(id, {...todos[index], title});
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={logo} className="App-logo" alt="logo" />
          </a>
          <h4>
            Todo List
          </h4>
        </header>
        <InputTodo addTodo={this.addTodo} />
        {
          this.state.todos.length ?
            <TotoList
              todos={this.state.todos}
              deleteTodo={this.deleteTodo}
              editTodo={this.editTodo}
            /> :
            <span>Loading...</span>
        }
      </div>
    );
  }
}

export default App;
