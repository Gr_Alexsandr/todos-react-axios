import React, {Component} from 'react';
import './todo-item.css';
import 'materialize-css';

class TodoItem extends Component {
  state = {
    checked: this.props.todo.completed,
    value: this.props.todo.title,
    edit: false
  };

  handleCheckbox = () => {
    const checked = !this.state.checked;
    this.setState({
      checked
    });
  };

  deleteTodo = () => {
    const {id} = this.props.todo;
    this.props.deleteTodo(id);
  };

  editTodo = () => {
    this.setState({
      edit: true
    });
  };

  handleInput = e => {
    this.setState({
      value: e.target.value
    });
  };

  changeTitleTodo = e => {
    if (e.key === 'Enter') {
      const {id} = this.props.todo;
      this.props.editTodo(id, this.state.value);

      this.setState({
        edit: false
      });
    }
  };

  render() {
    const {checked, edit} = this.state;

    return (
      <div>
        {
          edit ?
            <input
              type="text"
              value={this.state.value}
              onChange={this.handleInput}
              onKeyPress={this.changeTitleTodo}
            /> :
            <li className="todo-item">
              <div>
                <span>{this.props.index + 1}. </span>
                <label>
                  <input
                    type="checkbox"
                    checked={checked}
                    onChange={this.handleCheckbox}
                  />
                  <span className={checked ? 'checked' : ''}>{this.props.todo.title}</span>
                </label>
              </div>
              <div>
                <i
                  className="red-text text-darken-2 delete"
                  onClick={this.deleteTodo}
                >
                  delete
                </i>
                <i
                  className="green-text text-darken-2 edit"
                  onClick={this.editTodo}
                >
                  edit</i>
              </div>
            </li>
        }
      </div>
    );
  }
}

export default TodoItem;
