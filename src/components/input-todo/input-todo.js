import React, {Component} from 'react';
import './input-todo.css'

class InputTodo extends Component {
  state = {
    value: ''
  };

  handleChange = e => {
    this.setState({
      value: e.target.value
    });
  };

  addTodo = e => {
    if (e.key === 'Enter') {
      this.props.addTodo(this.state.value);

      this.setState({
        value: ''
      });
    }
  };

  render() {
    return (
      <div className="input-todo">
        <input
          type="text"
          placeholder="Add Todo"
          value={this.state.value}
          onChange={this.handleChange}
          onKeyPress={this.addTodo}
        />
      </div>
    );
  }
}

export default InputTodo;
