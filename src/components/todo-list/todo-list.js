import React, {Component} from 'react';
import TodoItem from '../todo-item';
import './todo-list.css'

class TotoList extends Component {
  render() {
    return (
      <ul className="todo-list">
        {
          this.props.todos.map((todo, index) => {
            return (
              <TodoItem
                key={todo.id}
                todo={todo}
                index={index}
                deleteTodo={this.props.deleteTodo}
                editTodo={this.props.editTodo}
              />
            );
          })
        }
      </ul>
    );
  }
}

export default TotoList;
