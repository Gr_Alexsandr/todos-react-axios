import axios from 'axios';

const URL = 'https://5b1e78344d4fc00014b07dd0.mockapi.io/api/todoreact';

export const getTodo = () => {
  return axios.get(URL).then(response => {
    return response.data;
  });
};

export const postTodo = obj => {
  return axios.post(URL, obj).then(response => {
    return response.data;
  });
};

export const deleteTodo = id => {
  return axios.delete(`${URL}/${id}`).then(response => {
    return response.data;
  });
};

export const putTodo = (id, obj) => {
  return axios.put(`${URL}/${id}`, obj).then(response => {
    return response.data;
  });
};
